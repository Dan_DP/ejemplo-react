import React, {useState, useEffect} from 'react';
import {Bar} from 'react-chartjs-2';

import datos from '../data.json';

function Barra(){
    const [chartData, setChartData] = useState({});

    const [data] = useState(datos);

    const chart = () => {
        setChartData({
            labels:meses(),
            datasets: [
                {
                    label: 'Consultas',
                    data: appointments(),
                    backgroundColor: "rgba(255,0,0,0.5)",
                    borderWidth: 4
                },
                {
                    label: 'Ingresos',
                    data: ingresos(),
                    backgroundColor: "rgba(0,255,128,0.5)",
                    borderWidth: 4
                }
            ]
        });
    }

    //Ordenando el arreglo por meses y devolviendo
    function meses() {
        let mesesArray = [];
        data.sort(function (a, b){return (a.year-b.year)||(a.month-b.month)});
        data.forEach((mesObj)=>{
            mesesArray.push(mesObj.month.toString()+"/"+mesObj.year.toString());
        })
        return mesesArray;
    }

    //Devolviendo las consultas
    function appointments() {
        let appointmentsArray = [];
        //data.sort(function (a, b){return (a.year-b.year)||(a.month-b.month)});
        data.forEach((mesObj)=>{
            appointmentsArray.push(mesObj.appointments);
        })
        return appointmentsArray;
    }

    //Devolviendo los ingresos
    function ingresos() {
        let revenueArray = [];
        //data.sort(function (a, b){return (a.year-b.year)||(a.month-b.month)});
        data.forEach((mesObj)=>{
            revenueArray.push(mesObj.revenue);
        })
        return revenueArray;
    }

    useEffect(()=>{
        chart()
    },[]);

    return(
        <div className="grafico">
            <div>
                <Bar data={chartData}/>
            </div>
            
        </div>
    );
}

export default Barra;