import React from 'react';
import moment from 'moment'

const validate = values => {
  const errors = {};

  if(!values.id) errors.id = 'Este campo es obligatorio';

  if(!values.fecha) errors.fecha = 'Este campo es obligatorio';
  else{
    let tmp = moment(values.fecha, 'DD/MM/YYYY',true);
    if(!tmp._isValid) errors.fecha = 'La fecha debe tener formato: DD/MM/YYYY';
  }

  if(!values.paciente) errors.paciente = 'Este campo es obligatorio';
  else{
    //let re = new RegExp('^([A-Z]|Ñ)([a-z]|ñ|[A-Z]|Ñ|\s)*$');
    var re = /^([A-Z]|Ñ)([a-zÀ-ÿA-ZÑñ]|\s)*$/;
    if(re.exec(values.paciente)===null) errors.paciente = 'El nombre del paciente debe comenzar con mayúscula';
  }

  if(!values.valorConsulta) errors.valorConsulta = 'Este campo es obligatorio';

  return errors;
}

class ConsultaForm extends React.Component {
    constructor () {
        super();
        this.state = {
          id: '',
          fecha: '',
          paciente: '',
          valorConsulta: 0,
          errors:{}
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      
    
      handleSubmit(e) {
        e.preventDefault();
        const {errors, ...sinErrors} = this.state;
        const result = validate(sinErrors);
        this.setState({errors:result});
        if(Object.keys(result).length) return;
        this.props.onAddRegistro(this.state);
        this.setState({
          id: '',
          fecha: '',
          paciente: '',
          valorConsulta: 0,
          errors:{}
        });
      }
    
      handleInputChange(e) {
        const {value, name} = e.target;
        //console.log(moment().format('dddd'));
        this.setState({
          [name]: value
        });
      }
    
      render() {
        const {errors} = this.state;
        return (
          <div className="card">
            <form onSubmit={this.handleSubmit} className="card-body">
              <div className="form-group">
                <input
                  type="text"
                  name="id"
                  className="form-control"
                  value={this.state.id}
                  onChange={this.handleInputChange}
                  placeholder="ID"
                  />
                  {errors.id && <span className="text-danger small">{errors.id}</span>}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="fecha"
                  className="form-control"
                  value={this.state.fecha}
                  onChange={this.handleInputChange}
                  placeholder="Fecha"
                  />
                  {errors.fecha && <span className="text-danger small">{errors.fecha}</span>}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="paciente"
                  className="form-control"
                  value={this.state.paciente}
                  onChange={this.handleInputChange}
                  placeholder="Paciente"
                  />
                  {errors.paciente && <span className="text-danger small">{errors.paciente}</span>}
              </div>
              <div className="form-group">
                <input
                  type="number"
                  name="valorConsulta"
                  className="form-control"
                  value={this.state.valorConsulta}
                  onChange={this.handleInputChange}
                  placeholder="Valor de consulta"
                  />
                  {errors.valorConsulta && <span className="text-danger small">{errors.valorConsulta}</span>}
              </div>
              <button type="submit" className="btn btn-primary">
                Registrar
              </button>
            </form>
          </div>
        )
      }
}

export default ConsultaForm;