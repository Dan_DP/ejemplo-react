import React from 'react';
import './App.css';

import Navigation from './components/Navigation';
import Bar from './components/Barra';
import ConsultaForm from './components/ConsultaForm';
import DataTable from 'react-data-table-component';

class App extends React.Component {

  constructor(){
    super();
    this.state={
      title:'Monitoreo Salud',
      consultas:[
        {
          id: 1,
          fecha: '02/09/2020',
          paciente: 'Vicente De Paz',
          valorConsulta: 10
        }
      ]
    }
    this.handleAddRegistro = this.handleAddRegistro.bind(this);
  }

  handleAddRegistro(registro) {
    //console.log(this.state.consultas, registro);
    this.setState({
      consultas: [...this.state.consultas, registro]
    })
  }
  
  render(){

    const columnas = [
      {
        name: 'ID',
        selector: 'id',
        sortable: true
      },
      {
        name: 'Fecha',
        selector: 'fecha',
        sortable: true
      },
      {
        name: 'Paciente',
        selector: 'paciente',
        sortable: true
      },
      {
        name: 'Valor',
        selector: 'valorConsulta',
        sortable: true
      },
      {
        name: 'Eliminar',
        cell: row => <button type="button" className="btn btn-danger my-2" onClick={()=>removeRegistro(row)}>Eliminar</button>,
      }
    ];

    const paginationOptions = {
      rowsPerPageText: 'Filas por página',
      rangeSeparator: 'de',
      selectAllRowsItem: true,
      selectAllRowsItemText: 'Todos'
    }

    let master = this;

    function removeRegistro(registro){
      //Obteniendo el índice del elemento a eliminar
      let index = master.state.consultas.findIndex(element => element.id===registro.id);
      master.state.consultas.splice(index,1);
      master.setState({
        consultas: master.state.consultas
      })
    }

    return (
      <div className="App">
        <Navigation titulo={this.state.title} numconsultas={this.state.consultas.length}/>
        
        <header className="App-header">
        <div className="container-fluid">
        <div>
          <h2>Consultas e ingresos por mes</h2>
          <div className="row mt-4 ">
          <div className="col-md-6 col-center bg-white rounded">
            <Bar/>
          </div>
          </div>
        </div>
        <div className="row mt-4">
        <div className="col-md-4 text-center">
                <p>
                {this.state.title}
              </p>
              <ConsultaForm onAddRegistro={this.handleAddRegistro}></ConsultaForm>
        </div>
        <div className="col-md-8">
          <div className="row mr-4 ml-4">
          <div className="table-responsive">
            <DataTable 
            columns={columnas}
            data={this.state.consultas}
            title="Consultas"
            paginationComponentOptions={paginationOptions}
            pagination
            fixedHeader
            fixedHeaderScrollHeight="600px"/>
          </div>
          </div>
        </div>
        </div>

        </div>
        </header>



      </div>
    );
  }
}

export default App;
